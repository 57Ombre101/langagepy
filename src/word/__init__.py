class Word:
    def __init__(self, writing, meaning, belonging, parent):
        #str, way of writing the word (identifies it)
        self.writing = writing
        #meaning of the word
        self.meaning = meaning
        #belongs to which language
        self.belonging = belonging
        # parent : either Word, or null
        self.parent = parent

    def __str__(self):
        return self.writing

    def transmit(self):
        """Transmits the word to another character

For now, the word transmitted is the same, but it will change"""
        list_letters = [letter for letter in self]
        writing = "".join(list_letters)
        if writing != self.writing: # for now, it's the same as self
            word = Word(writing, self.meaning, self.belonging, self)
        else:
            word = self
        return word
