"""
Part of the action module managing the speak
"""

def tell(speaker, listener, sentence):
    words = sentence.split(" ")
    #print("Words : ", words)
    meaning = []
    for w in words:
        for voc in listener.vocabulary:
            if voc.writing == w:
                meaning.append(voc.meaning)
    #print(meaning)
    for m in meaning:
        #print(m)
        if m == "give":
            if len(meaning) == 2:
                t = meaning.index(m)
                m2 = meaning[1-t]
                if m2 == "stone":
                    listener.stone += 1
                    speaker.stone -= 1
                elif m2 == "wood":
                    listener.wood += 1
                    speaker.wood -= 1
                elif m2 == "food":
                    listener.food += 1
                    speaker.food -= 1
                #else:
                    #print("Did not understand")
            #else:
                #print("Did not understand")
        elif m == "request":
            if len(meaning) == 2:
                t = meaning.index(m)
                m2 = meaning[1-t]
                #print(m2)
                if m2 in ["stone", "wood", "food"]:
                    #print("Added request")
                    listener.requests.append((speaker.name, m2, 5))
                #else:
                    #print("Did not understand")
        #else:
        #    print("Unknown request")
    return speaker, listener
