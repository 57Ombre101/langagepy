"""
This module provides functions to animate the world.
"""

import random as rd
from src.tools import find_person
from src.action.speak import tell


def give(giver, receiver, material):
    """giver and receiver are Characters. material is a str."""
    if material == "stone":
        assert giver.stone > 0
        for w in giver.vocabulary:
            if w.meaning == "stone":
                wstone = w.writing
            if w.meaning == "give":
                wgive = w.writing
        sentence = wstone + " " + wgive
        
    elif material == "wood":
        assert giver.wood > 0
        for w in giver.vocabulary:
            if w.meaning == "wood":
                wwood = w.writing
            if w.meaning == "give":
                wgive = w.writing
        sentence = wwood + " " + wgive

    elif material == "food":
        assert giver.food > 0
        for w in giver.vocabulary:
            if w.meaning == "food":
                wfood = w.writing
            if w.meaning == "give":
                wgive = w.writing
        sentence = wfood + " " + wgive

    print("give", giver.name, " ", receiver.name, " ", material)
    return tell(giver, receiver, sentence)
        

def step(people, links):
    """Make one step."""
    for person in people:
        # Produces what it needs
        person = production(person)


        #countdown for requests
        # a bit bugged (but not that important) :
        # it decrements the requests made at this step
        # by people called before the person
        reqtmp = []
        for req in person.requests:
            if req[2] > 0:
                reqtmp.append((req[0], req[1], req[2] -1))
        person.requests = reqtmp
        
        # Then, two choices (random):
        #  * request a resource
        #  * give a resource (after a request)
        choice = rd.random()
        if choice < 0.5 and person.requests != []:
            # giving :
            # conditions :
            #  * request with this resource
            #  * resource > 1. This limit is an important parameter to study.
            check = False #check if person has already given
            for r in person.requests:
                material = r[1]
                receiver = find_person(people, r[0])
                
                if not(check) and material == "stone" and person.stone > 1:
                    check = True
                    t = person.requests.index(r)
                    give(person, receiver, "stone")
                elif not(check) and material == "wood" and person.wood > 1:
                    check = True
                    t = person.requests.index(r)
                    give(person, receiver, "wood")
                elif not(check) and material == "food" and person.food > 1:
                    check = True
                    t = person.requests.index(r)
                    give(person, receiver, "food")
            if check:
                person.requests.pop(t)
        else: #request
            person, neighbour = request(person, people, links)
            if not((person, neighbour) == (None, None)):
                #need to also actualize neighbour
                for k in range(len(people)):
                    tempp = people[k]
                    if tempp.name == person.name:
                        people[k] = person
                        #print(people[k])
                    elif tempp.name == neighbour.name:
                        people[k] = neighbour
                        #print(people[k])
                    
    return people, links

def production(person):
    job = person.job
    if job == "stoner":
        person.stone += 1
    elif job == "wooder":
        person.wood += 1
    elif job == "fooder":
        person.food += 1
    else:
        print("Error, job unknown")
    return person

def choose_neighbour_random(person, people, links):
    """person : Character object
people : list of Characters
links : SetList object

return selected : Character object
"""
    neighbours = [] #list of names
    for bond in links.links:
        for i in [0,1]:
            if bond[i] == person.name:
                #print("link ", person.name, "-", bond[1-i])
                neighbours.append(bond[1-i])
    n = len(neighbours)
    #print(person.name)
    nbrd = rd.randrange(n)
    for chartmp in people:
        if chartmp.name == (neighbours[nbrd]):
            selected = chartmp
    return selected

def choose_resource_request(person):
    """Defines which resource to ask for."""
    s = person.stone
    w = person.wood
    f = person.food
    if s == w and w == f:
        nbrd = rd.randrange(3)
        if nbrd == 0:
            return "stone"
        elif nbrd == 1:
            return "wood"
        else:
            return "food"
    elif s == min(s, w, f):
        if w == s:
            nbrd = rd.randrange(2)
            if nbrd == 0:
                return "stone"
            else:
                return "wood"
        elif f == s:
            nbrd = rd.randrange(2)
            if nbrd == 0:
                return "stone"
            else:
                return "wood"
        else:
            return "stone"
    elif w == min(s, w, f):
        if f == w:
            nbrd = rd.randrange(2)
            if nbrd == 0:
                return "wood"
            else:
                return "food"
        else:
            return "wood"

    else:
        return "food"

def request(person, people, links):
    # if no neighbour, nothing happens
    neighbours = [] #list of names
    for bond in links.links:
        for i in [0,1]:
            if bond[i] == person.name:
                #print("link ", person.name, "-", bond[1-i])
                neighbours.append(bond[1-i])
    if len(neighbours) == 0:
        return (None,None)

    
    selected = choose_neighbour_random(person, people, links)
    #choose resource to ask for
    r = choose_resource_request(person)
    sentence = ""
    for x in person.vocabulary:
        if x.meaning == "request":
            sentence += x.writing + " "
        if x.meaning == r:
            sentence += x.writing + " "
    person, selected = tell(person, selected, sentence.strip(" "))
    return person, selected

