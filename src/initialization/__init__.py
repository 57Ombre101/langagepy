"""
Initialization module
"""
from src.character import Character
from src.word import Word
from src.linking import SetLinks


#First configuration

##Words

stone1 = Word("shelak", "stone", "Vijniak", None)
wood1 = Word("ajn", "wood", "Vijniak", None)
food1 = Word("avil", "food", "Vijniak", None)
request1 = Word("krei", "request", "Vijniak", None)
give1 = Word("lavi", "give", "Vijniak", None)


vijniak = [stone1, wood1, food1, request1, give1]

##Characters

unu = Character("Unu", vijniak.copy(), "Vijniak", "fooder")
du = Character("Du", vijniak.copy(), "Vijniak", "stoner")
tri = Character("Tri", vijniak.copy(), "Vijniak", "stoner")
kvar = Character("Kvar", vijniak.copy(), "Vijniak", "stoner")

kvin = Character("Kvin", vijniak.copy(), "Vijniak", "fooder")
ses = Character("Ses", vijniak.copy(), "Vijniak", "fooder")
sep = Character("Sep", vijniak.copy(), "Vijniak", "fooder")

ok = Character("Ok", vijniak.copy(), "Vijniak", "wooder")
naŭ = Character("Naŭ", vijniak.copy(), "Vijniak", "wooder")
dek = Character("Dek", vijniak.copy(), "Vijniak", "stoner")
dekunu = Character("Dek-unu", vijniak.copy(), "Vijniak", "wooder")

dekdu = Character("Dek-du", vijniak.copy(), "Vijniak", "wooder")


links = SetLinks([("Unu", "Du"), ("Unu", "Tri"), ("Unu", "Kvar"), ("Du", "Tri"), ("Du", "Kvar"), ("Du", "Naŭ"), ("Tri", "Kvar"), ("Tri", "Sep"), ("Tri", "Ok"), ("Tri", "Dek-du"), ("Kvin", "Ses"), ("Kvin", "Sep"), ("Ses", "Sep"), ("Sep", "Ok"), ("Ok", "Naŭ"), ("Ok", "Dek"), ("Ok", "Dek-unu"), ("Naŭ", "Dek"), ("Naŭ", "Dek"), ("Dek", "Dek-unu")])

people = [unu, du, tri, kvar, kvin, ses, sep, ok, naŭ, dek, dekunu, dekdu]
    
