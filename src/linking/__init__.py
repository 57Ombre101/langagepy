"""
Links people together.
"""

class SetLinks:
    def __init__(self, links):
        self.links = links
        self.index = 0

    def __str__(self):
        string = ""
        for x in self.links:
            string = string + x[0] +"<->"+x[1] + "\n"
        return string

    def __iter__(self):
        return self
    
    def __next__(self):
        if self.index == len(self.links) -1:
            self.index =0
            raise StopIteration
        self.index = self.index + 1
        return self.links[self.index]

    def append(self, link):
        self.links.append(link)

    def parcours(self):
        for i in range(len(self.links)):
            yield self.links[i]
