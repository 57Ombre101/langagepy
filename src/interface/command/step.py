from src.interface.command import Command
from src.action import step

class Step(Command):
    """ """
    def action(self, people, links):
        return step(people, links)
