import argparse

from src.interface.command import Command

class Show(Command):
    """ """
    def action(self, people, links, arg):
        if arg != "":
            blinks,namelist = parse_show(arg)
            if blinks:
                print(links)
            else:
                for person in people:
                    for name in namelist:
                        if person.name == name:
                            print(person)
                            print(person.requests)
        else: #default case, print all
            for person in people:
                print(person)

def parse_show(args):
    """Usage :
    show : print summary
    show <name> : print person with name
"""
    args = args.split(" ")
    parser_show = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)

    parser_show.add_argument("name", help = "Name", nargs='*', default=None)
    parser_show.add_argument("-l", help = "Links", action='store_true', default=False)
    args = parser_show.parse_args(args)
    return (args.l,args.name)
