import cmd
import argparse
from src.initialization import links, people
from src.saving import save_all, load_all
from src.character import Character
from src.initialization import vijniak

from src.interface.command.step import Step
from src.interface.command.show import Show

class Shell(cmd.Cmd):
    intro = "Language stuff"
    prompt = "> "

    def __init__(self):
        """Init the shell"""
        cmd.Cmd.__init__(self)

    def do_step(self, arg):
        """Executes one step"""
        step = Step()
        global people, links
        people,links = step.action(people, links)

    def do_show(self, arg):
        """show
show <name>
[Not done]show -l

Show something:
Whole lot of people, one in particular, a word...
"""
        global people, links
        show = Show()
        show.action(people, links, arg)

    def do_create(self, arg):
        """[Undone]
Syntax :
`create`


For later :

create char <name> <job>
create link <name1> <name2>

Add a character, or a link"""
        global people, links
        # parse_create(arg)
        t = False
        while not(t):
            obj_type= input("C, L ? ")
            if obj_type in ["C", "c", "L", "l"]:
                t = True
        if obj_type in ("C", "c"):
            t = False
            while not(t):
                char_name = input("Name : ")
                char_job = input("Job : ")
                confirm = input("Confirm ? (y or n) ")
                if confirm == "y" and char_job in ["wooder", "stoner", "fooder"]:
                    t = True
                elif confirm != "y":
                    t == False # should be useless
                elif not(char_job in ["wood", "stone", "food"]):
                    print("Job invalid !")
            new_char = Character(char_name, vijniak, "Vijniak", char_job)
            people.append(new_char)
        elif obj_type in ("L", "l"):
            t = False
            while not(t):
                char_1 = input("Char 1 : ")
                char_2 = input("Char 2 : ")
                confirm = input("Confirm ? (y or n) ")
                ppl_names = [char.name for char in people]
                if confirm == "y" and char_1 in ppl_names and char_2 in ppl_names:
                    t = True
                elif not(char_1 in ppl_names and char_2 in ppl_names):
                    print("One of the name is invalid !")
            links.append((char_1, char_2))
                    
        
    def do_save(self, arg):
        """save <savename>

Saves data in save/<savename>.sav"""
        save_all(arg, (people, links))

    def do_load(self, arg):
        """load <savename>

Load data from save/<savename>.sav"""
        global people, links
        people, links = load_all(arg)

    def do_init(self, arg):
        """[Undone]
Initializes"""
        
    def do_exit(self, arg):
        """Exits"""
        return True

    def do_remove(self, arg):
        """ """
        pass


def pre_parse(arg):
    return arg.split(" ")

def parse_create(args):
    """Usage : see do_create"""
    args = pre_parse(args)
    parser_create = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    parser_create.add_argument("type", help = "Type of ", choices=["char", "link"])
    parser_create.add_argument("name", help = "Name")
    args = parser_create.parse_args(args)
    return args
