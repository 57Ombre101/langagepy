class Character:
    def __init__(self, name, vocabulary, community, job):
        self.name = name
        self.vocabulary = vocabulary
        self.community = community
        self.stone = 0
        self.wood = 0
        self.food = 0
        self.job = job
        # request : (name person, what it wants, countdown)
        # when countdown = 0: request deleted
        self.requests = []

    def __str__(self):
        string = self.name + ": " + "s:" + str(self.stone) + " w:" + str(self.wood) + " f:" + str(self.food) + "|"
        for k in range(len(self.vocabulary)-1):
            string += str(self.vocabulary[k]) + ", "
        string += str(self.vocabulary[-1])
        return string
