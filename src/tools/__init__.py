"""
Tool module provides various utilities.
"""

def find_person(people, name):
    for person in people:
        if person.name == name:
            return person
    print("Person not found !")

dict_validate = {
    'yes' : ["yes", "y", "Yes", "Y"],
    'no' : ["no", "n", "No", "N"]
    }
