"""
Manages loading and saving the data.

[Before 2017-07 :]Part of the code taken from Broken's World Adventurer
"""

import os
import pickle
from src.tools import dict_validate

def save_all(savename, data):
    """Undone"""
    listdir = os.listdir("./save")
    if not(savename + ".sav" in listdir):
        # if the savefile doesn't exist, it is created
        save(data, "save/" + savename + ".sav")
    else:
        # it already exists
        b = True # bool to check wether the user has validated
        # I could have modified b, so that if `no` or `yes`,
        # b changes value, which exits the loop, and after I'd
        # put `return`
        # but well, nice enough ?
        while b:
            t = input("Savefile already exists. Overwrite ? ")
            # if t = Y,y , Yes, yes...
            if t in dict_validate['yes']:
                save(data, "save/" + savename + ".sav")
                return True
            # nasty thought : replacing `return True` by
            # `return not(b)
            elif t in dict_validate['no']:
                return True
            else:
                print("Please enter y or n")

def load_all(savename):
    """ """
    listdir = os.listdir("./save")
    nametotal = savename + ".sav"
    if not(nametotal in listdir):
        print("Save does not exist")
    else:
        return load("./save/" + nametotal)



def save(obj, file):
    """Save the object obj"""
    with open(file, 'wb') as f:
        p = pickle.Pickler(f)
        p.dump(obj)

def load(file):
    with open(file, 'rb') as f:
        u = pickle.Unpickler(f)
        return u.load()
